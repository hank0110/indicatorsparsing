﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IndicatorsParsing.Models;

namespace IndicatorsParsing
{
    class Program
    {
        static NewsEntities db = new NewsEntities();
        public static List<string> EngDateNationList
        {
            get
            {
                return new List<string> {
                    "加拿大",
                    "新加坡",
                    "日本",
                    "澳大利亞",
                    "香港"
                };
            }
        }

        static void Main(string[] args)
        {

            if (args.Length == 1)
                Parsing(args[0]);
            else
                Parsing();
        }

        private static void Parsing(string path = @"D:\專案\IndicatorsParsing\IndicatorsParsing\csv\")
        {
            var fileList = Directory.GetFiles(path, "*.csv");
            var sqlList = new List<string>();

            var nation = string.Empty;
            var indicators = string.Empty;

            var errDate = DateTime.Now;

            var indicatorsDataList = new List<IndicatorsData>();

            foreach (var file in fileList)
            {
                var Lines = File.ReadLines(file, Encoding.Default);
                
                sqlList.Clear();

                nation = string.Empty;
                indicators = string.Empty;

                errDate = DateTime.Now;

                
                foreach (var item in Lines)
                {
                    var data = item.Replace("\"", "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    if (data.Length >= 2)
                    {
                        // 串起來千分位數值
                        if (data.Length > 2)
                        {
                            for (int i = 2; i < data.Length; i++)
                                data[1] += data[i];
                        }

                        if (!DateTime.TryParse(data[0], out errDate))    // indicators data
                        {
                            WriteIndicatorsSQL(ref indicatorsDataList, sqlList);

                            if (EngDateNationList.Contains(data[0].Replace("*", "")))
                                nation = data[0].Replace("*", "");
                            else
                                nation = data[0];

                            indicators = data[1];

                            // ef delete data
                            db.tb_prosperity_temp.RemoveRange(db.tb_prosperity_temp.Where(o => o.Nation == nation && o.Name == indicators));
                            db.SaveChanges();

                            sqlList.Add(
                            string.Format(
                                @"delete from tb_prosperity where Nation='{0}' and Name='{1}';",
                                nation,
                                indicators
                            ));
                        }
                        else                                            // indicators info
                        {
                            if (EngDateNationList.Contains(nation))
                                data[0] = DateTime.ParseExact(data[0], "MMM-yy", CultureInfo.InvariantCulture).ToString("yyyy/MM");
                            else
                                data[0] = data[0].Replace("-", "/");

                            indicatorsDataList.Add(new IndicatorsData() {
                                date = data[0],
                                nation = nation,
                                name = indicators,
                                val = data[1]
                            });
                            
                        }

                    }
                    else
                    {
                        Console.WriteLine("err");
                        return;
                    }
                }

                WriteIndicatorsSQL(ref indicatorsDataList, sqlList);

                //Console.WriteLine(inputStr);
                System.IO.File.WriteAllLines(file.Replace(@"\csv\", @"\txt\").Replace(".csv", ".txt"), sqlList);

                Console.WriteLine(nation + " finished!");

            }
        }

        #region WriteIndicatorsSQL
        private static void WriteIndicatorsSQL(ref List<IndicatorsData> idcList, List<string> sqlList)
        {
            if (GetIsQuarterDate(idcList))
            {
                SetQuarterDate(ref idcList);
            }

            sqlList.AddRange(GetInsertIndicatorsSQL(idcList));
            idcList.Clear();
        } 
        #endregion

        #region GetInsertIndicatorsSQL
        private static List<string> GetInsertIndicatorsSQL(List<IndicatorsData> indicatorsDataList)
        {
            var list = new List<string>();
            var prosperityList = new List<tb_prosperity_temp>();

            foreach (var item in indicatorsDataList)
            {
                // ef insert data
                prosperityList.Add(new tb_prosperity_temp() { Year_Month = item.date, Nation = item.nation, Name = item.name, Value_Index = item.val });

                list.Add(
                    string.Format(
                        @"insert into tb_prosperity (Year_Month, Nation, Name, Value_Index) values ('{0}', '{1}', '{2}', '{3}');",
                        item.date,
                        item.nation,
                        item.name,
                        item.val
                    ));
            }

            db.tb_prosperity_temp.AddRange(prosperityList);
            db.SaveChanges();

            return list;
        } 
        #endregion

        #region GetIsQuarterDate
        private static bool GetIsQuarterDate(List<IndicatorsData> indicatorsDataList)
        {
            if (indicatorsDataList.Count <= 0)
                return false;

            foreach (var idc in indicatorsDataList)
            {
                var idc_split = idc.date.Split(new char[] { '/' });
                if (!(idc_split.Length == 2 && Array.Exists(QuarterDate.Month.ToArray(), o => o.Equals(idc_split[1]))))
                    return false;
            }

            return true;
            //indicatorsDataList.Clear();
        } 
        #endregion

        #region SetQuarterDate
        private static void SetQuarterDate(ref List<IndicatorsData> indicatorsDataList)
        {
            foreach (var item in indicatorsDataList)
            {
                for (int i = 0; i < QuarterDate.Month.Count; i++)
                {
                    item.date = item.date.Replace("/" + QuarterDate.Month[i], QuarterDate.Qname[i]);
                }
            }
        } 
        #endregion

        #region IndicatorsData
        public class IndicatorsData
        {
            public string date { get; set; }
            public string nation { get; set; }
            public string name { get; set; }
            public string val { get; set; }
        } 
        #endregion

        #region QuarterDate
        public class QuarterDate
        {
            public static List<string> Month
            {
                get
                {
                    return new List<string> { "03", "06", "09", "12" };
                }
            }

            public static List<string> Qname
            {
                get
                {
                    return new List<string> { "Q1", "Q2", "Q3", "Q4" };
                }
            }
        } 
        #endregion
    }
}
